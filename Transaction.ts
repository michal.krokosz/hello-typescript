export abstract class Transaction {
    abstract readonly transactionName: string;
    constructor(private readonly amount: number) {
        this.amount = amount;
    }

    getAmount(): number {
        return this.amount;
    }
}

export class SaleTransaction extends Transaction {
    transactionName = "SALE";
}


export class RefundTransaction extends Transaction {
    transactionName = "REFUND";
}

export class FollowUpSaleTransaction extends SaleTransaction {
    transactionName = "FOLLOW-UP SALE";
    private isApproved: boolean | undefined;
    referencedTransaction: SaleTransaction;
    constructor(amount: number, referencedTransaction: SaleTransaction) {
        super(amount);
        this.referencedTransaction = referencedTransaction;
    }
    getOriginalAmount(): number {
        return this.referencedTransaction.getAmount();
    }

    isTrxApproved(): boolean {
        return (this.isApproved ?? true);
    }
}


const saleTrx = new SaleTransaction(100);
const followUpSaleTrx = new FollowUpSaleTransaction(300, saleTrx);

console.log(`trx name: ${saleTrx.transactionName}`);
console.log(`follow-up transaction amount is: ${followUpSaleTrx.getAmount()}`);
console.log(`original amount is: ${followUpSaleTrx.getOriginalAmount()}`);

