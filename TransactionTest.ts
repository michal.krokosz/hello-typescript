import { FollowUpSaleTransaction, RefundTransaction, SaleTransaction, Transaction } from "./Transaction";
import { expect } from "chai";
import { spy, verify } from "ts-mockito";

describe ('Transaction', () => {
    it(`returns transaction amount and name`, () => {
        const saleTrx: Transaction[] = [new SaleTransaction(123), new RefundTransaction(234)];
        expect(saleTrx[0].transactionName).to.eq("SALE");
        expect(saleTrx[1].transactionName).to.eq("REFUND");
    });
});

describe ('FollowUpSaleTransaction', () => {
    it(`returns original transaction amount`, () => {
        const saleTrx = new SaleTransaction(123);
        const saleTrxSpy = spy(saleTrx);
        const followUpSaleTransaction = new FollowUpSaleTransaction(124, saleTrx);
        expect(followUpSaleTransaction.getOriginalAmount()).to.eq(123);
        verify(saleTrxSpy.getAmount()).once();
    });
});
